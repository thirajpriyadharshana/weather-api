<?php

namespace App\Http\Controllers;

use App\Services\Weather\WeatherServiceInterface;
use Illuminate\Support\Facades\Validator;

/**
 * Class WeatherController
 * @package App\Http\Controllers
 */
class WeatherController extends Controller
{
    /**
     * @var WeatherServiceInterface
     */
    private $weatherService;

    /**
     * WeatherController constructor.
     * Injected WeatherService
     * @param WeatherServiceInterface $weatherService
     */
    public function __construct(WeatherServiceInterface $weatherService)
    {
        $this->weatherService = $weatherService;
    }

    /**
     * @return mixed
     */
    public function getCurrentWeather()
    {
        $validQueryParam = Validator::make(request()->all(), [
           'q' => 'required'
        ]);

        if ($validQueryParam->fails()) {
            return response()->json([
                'message' => 'Required fields are missing',
                'status' => false,
                'errors' => $validQueryParam->errors(),
            ], 422);
        }

        $weatherData = $this->weatherService->fetchWeatherDetail(request()->all());
        return response()->json([
            'data' => $weatherData['data'],
            'message' => $weatherData['message'],
            'success' => $weatherData['success'],
        ], $weatherData['httpStatusCode']);
    }
}
