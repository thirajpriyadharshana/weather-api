<?php


namespace App\Services\Weather\DataSources\Adaptors\WeatherApi;


use App\Services\Weather\DataSources\AdaptorInterface;
use App\Services\Weather\DataSources\APIClient\APIClient;
use App\Services\Weather\DataSources\APIClient\APIResponse;

/**
 * Class WeatherApi
 * Adaptor for selected API
 *
 * @package App\Services\Weather\DataSources\Adaptors\WeatherApi
 */
class WeatherApi implements AdaptorInterface
{
    /**
     * @var APIClient
     */
    private $apiClient;

    /**
     * WeatherApi constructor.
     */
    public function __construct()
    {
        $this->apiClient = new APIClient(config('api.weather_api.base_url'));
    }

    /**
     * Fetching current weather details using relevant api
     *
     * @param array $args
     * @return APIResponse
     * @throws \Exception
     */
    public function fetchCurrentWeather(array $args) : APIResponse
    {
        $queryParams = array_merge(['key' => config('api.weather_api.api_key')], $args);
        return $this->apiClient->setEndpoint('current.json')->setQueryParams($queryParams)->getRequest();
    }
}
