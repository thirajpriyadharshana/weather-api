<?php


namespace App\Services\Weather\DataSources\APIClient;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Class APIClient
 * Wrapper for Guzzle to consume apis
 * @package App\Services\Weather\DataSources\APIClient
 */
final class APIClient
{
    /**
     * @var Client
     */
    protected $apiClient;
    /**
     * @var
     */
    protected $baseUrl;
    /**
     * @var
     */
    protected $endpoint;
    /**
     * @var
     */
    protected $headers;
    /**
     * @var
     */
    protected $body;
    /**
     * @var
     */
    protected $queryParams;

    /**
     * APIClient constructor.
     * @param string $baseUrl
     */
    public function __construct(string $baseUrl)
    {
        $this->apiClient = new Client([
            'base_uri' => $baseUrl
        ]);
    }

    /**
     * @return APIResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function postRequest() : APIResponse
    {

        try {
            $response = $this->apiClient->post($this->endpoint, [
                "headers" => $this->getHeaders(),
                "form_params" => $this->body,
            ]);

            $resBody = $response->getBody()->getContents();
            $resStatus = $response->getStatusCode();
            $resBody = json_decode($resBody);

            return new APIResponse($resBody, '', true, $resStatus);
        } catch (RequestException $e) {
            if (empty($e->getResponse())) {
                throw new \Exception('API connection error');
            }
            return new APIResponse([], json_decode($e->getResponse()->getBody()->getContents()), false);
        } catch (\Exception $exception) {
            return new APIResponse([], "Something went wrong", false, $exception->getCode());
        }

    }

    /**
     * @return APIResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getRequest() : APIResponse
    {
        try {
            $response = $this->apiClient->get($this->endpoint, [
                "query" => $this->queryParams,
                "headers" => $this->getHeaders(),
            ]);

            $resBody = $response->getBody()->getContents();
            $resStatus = $response->getStatusCode();
            $resBody = json_decode($resBody, true);

            return new APIResponse($resBody, '', true, $resStatus);

        } catch (RequestException $exception) {
            if (empty($exception->getResponse())) {
                throw new \Exception('Connection problem. Please try again');
            }
            return new APIResponse([], $exception->getResponse()->getReasonPhrase(), false, $exception->getCode());
        } catch (\Exception $exception) {

            return new APIResponse([], "Something went wrong", false, $exception->getCode());
        }
    }

    /**
     * @param mixed $baseUrl
     * @return APIClient
     */
    public function setBaseUrl($baseUrl): self
    {
        $this->baseUrl = $baseUrl;
        return $this;
    }

    /**
     * @param string $endpoint
     * @return APIClient
     */
    public function setEndpoint(string $endpoint): self
    {
        $this->endpoint = $endpoint;
        return $this;
    }

    /**
     * @param array $body
     * @return $this
     */
    public function setBody(array $body): self
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @param array $queryParams
     * @return self
     */
    public function setQueryParams(array $queryParams) : self
    {
        $this->queryParams = $queryParams;
        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders() : array
    {
        return $this->headers = array_merge($this->headers ?? [], $this->getDefaultHeaders());
    }

    /**
     * @param array $headers
     * @return $this
     */
    public function setHeaders(array $headers): self
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @return array
     */
    public function getDefaultHeaders() : array
    {
        return [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ];
    }

}
