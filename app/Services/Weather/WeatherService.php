<?php


namespace App\Services\Weather;


use App\Services\Weather\DataSources\Adaptors\AdaptorMapper;
use App\Services\Weather\DataSources\DataSourceHandler;

/**
 * Class WeatherService
 * Service class used for isolate business logic from the controller
 *
 * @package App\Services\Weather
 */
class WeatherService implements WeatherServiceInterface
{
    /**
     * @var DataSourceHandler
     */
    protected $dataSourceHandler;

    /**
     * WeatherService constructor.
     * Inject DataSourceHandler as a dependency
     *
     * @param DataSourceHandler $dataSourceHandler
     */
    public function __construct(DataSourceHandler $dataSourceHandler)
    {
        $this->dataSourceHandler = $dataSourceHandler;
    }

    /**
     * @param array $filters
     * @return array
     */
    public function fetchWeatherDetail(array $filters = []) : array
    {
        return $this->dataSourceHandler
            ->setAdaptor(AdaptorMapper::WEATHER_API)
            ->setFilterParams($filters)
            ->fetchCurrentWeather()->toArray();
    }
}
