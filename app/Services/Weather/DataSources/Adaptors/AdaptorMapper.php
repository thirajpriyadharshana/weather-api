<?php


namespace App\Services\Weather\DataSources\Adaptors;


use App\Services\Weather\DataSources\Adaptors\WeatherApi\WeatherApi;

/**
 * Class AdaptorMapper
 * Map adaptors according to the datasource handler
 *
 * @package App\Services\Weather\DataSources\Adaptors
 */
class AdaptorMapper
{
    /**
     *
     */
    public const WEATHER_API = 'weather_api';

    /**
     * List of adaptors available in the system
     * Currently only one adaptor available for selected api
     * @var string[]
     */
    public static $adaptors = [
        self::WEATHER_API => WeatherApi::class
    ];

    /**
     * Return adaptor class if available
     * @return mixed
     */
    public static function getAdaptor($adaptor)
    {
        if (isset(self::$adaptors[$adaptor])) {
            return self::$adaptors[$adaptor];
        }

        throw new \Exception('Invalid adaptor');
    }


}
