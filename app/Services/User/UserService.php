<?php

namespace App\Services\User;

use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserService
 * Service class used for isolate business logic from the controller
 *
 * @package App\Services\User
 */
class UserService implements UserServiceInterface
{
    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * UserService constructor.
     * Inject UserRepositoryInterface as a dependency
     *
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param array $requestData
     * @return mixed
     */
    public function registerUser(array $requestData)
    {
        $userData = [
            'name' => $requestData['name'],
            'email' => $requestData['email'],
            'password' => Hash::make($requestData['password'])
        ];
        return $this->userRepository->createUser($userData);
    }
}
