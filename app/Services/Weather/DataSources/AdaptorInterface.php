<?php


namespace App\Services\Weather\DataSources;


interface AdaptorInterface
{
    public function fetchCurrentWeather(array $args);
}
