<?php


namespace App\Repositories\User;


use App\Models\User;

/**
 * Class UserRepository
 * @package App\Repositories\User
 */
class UserRepository implements UserRepositoryInterface
{
    /**
     * @var User
     */
    protected $user;

    /**
     * UserRepository constructor.
     * Inject dependency (User Model) on constructing the repo class
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param array $requestData
     * @return User
     *
     * Storing user in DB using eloquent model
     */
    public function createUser(array $requestData): User
    {
        return $this->user->create($requestData);
    }
}
