<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\WeatherController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Group route with a prefix for API versioning
 */
Route::prefix('v1')->group(function () {

    /**
     * Public Route
     */
    Route::controller(AuthController::class)->group(function () {
        Route::post('register', 'registerUser');
        Route::post('login', 'loginUser');
    });

    /**
     * Protected Routes
     * Used Laravel Sanctum for Authentication
     */
    Route::middleware('auth:sanctum')->controller(WeatherController::class)->group(function () {
        Route::get('weather/current', 'getCurrentWeather')->name('weather.get.current');
    });

});

