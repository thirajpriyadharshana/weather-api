<?php

/**
 * Config values for third party apis
 */
return [
    'weather_api' => [
        'base_url' => env('WEATHER_API_BASE_URL', ''),
        'api_key' => env('WEATHER_API_KEY', '')
    ]
];
