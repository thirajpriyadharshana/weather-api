<?php


namespace App\Services\Weather\DataSources;


use App\Services\Weather\DataSources\Adaptors\AdaptorMapper;

/**
 * Class DataSourceHandler
 * This use for managing data sources.
 * The purpose of this approach is to make the functionality more scalable
 * Using this can use different third party apis and switch them easily using adaptors
 *
 * @package App\Services\Weather\DataSources
 */
class DataSourceHandler
{
    /**
     * @var
     */
    protected $adaptor;
    /**
     * @var
     */
    protected $filterParams;


    /**
     * @param mixed $adaptor
     * @return DataSourceHandler
     */
    public function setAdaptor($adaptor)
    {
        $adaptorClass = AdaptorMapper::getAdaptor($adaptor);
        $this->adaptor = new $adaptorClass();
        return $this;
    }

    /**
     * @param mixed $filterParams
     * @return DataSourceHandler
     */
    public function setFilterParams(array $filterParams): self
    {
        $this->filterParams = $filterParams;
        return $this;
    }

    /**
     * @return mixed
     */
    public function fetchCurrentWeather()
    {
        return $this->adaptor->fetchCurrentWeather($this->filterParams);
    }
}
