

## Weather API

This simple API create as Code Test using Laravel 9 framework. It provides weather details fetched from [weatherapi.com](http://weatherapi.com)

## API Documentation

Can access postman API [documentation](https://documenter.getpostman.com/view/1759694/Uz5Ardjm) and it has detail explanation about endpoints

## Hosting details

Application is hosted on AWS using EC2 instance. 
Link: http://ec2-18-132-73-1.eu-west-2.compute.amazonaws.com/
