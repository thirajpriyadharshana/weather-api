<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class WeatherApiTest extends TestCase
{
    use DatabaseMigrations;

    private const ROUTE_GET_CURRENT_WEATHER = 'weather.get.current';

    /**
     * Test valid request for get current weather
     *
     * @return void
     */
    public function test_can_get_current_weather_with_query_param()
    {
        // Preparation
        $this->withoutExceptionHandling();
        $user = User::factory()->create();

        // Performing
        $response = $this->actingAs($user)->getJson(route(self::ROUTE_GET_CURRENT_WEATHER, [ 'q' => 'London']));

        // Prediction
        $response->assertStatus(200)
            ->assertJsonPath('data.location.name', 'London');
    }

    /**
     * Test with invalid request
     * Without query params for location
     *
     * @return void
     */
    public function test_can_get_current_weather_without_query_param()
    {
        // Preparation
        $this->withoutExceptionHandling();
        $user = User::factory()->create();

        // Performing
        $response = $this->actingAs($user)->getJson(route(self::ROUTE_GET_CURRENT_WEATHER));

        // Prediction
        $response->assertStatus(422);
    }

    /**
     * Test with invalid request
     * Without query params for location
     *
     * @return void
     */
    public function test_can_get_current_weather_with_invalid_query_param()
    {
        // Preparation
        $this->withoutExceptionHandling();
        $user = User::factory()->create();

        // Performing
        $response = $this->actingAs($user)->getJson(route(self::ROUTE_GET_CURRENT_WEATHER, ['q' => 'asd6d868asd']));

        // Prediction
        $response->assertStatus(400)
            ->assertJsonPath('success', false);
    }
}
