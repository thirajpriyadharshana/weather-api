<?php

namespace App\Http\Controllers;

use App\Services\User\UserServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
    /**
     * @var UserServiceInterface
     */
    public $userService;

    /**
     * AuthController constructor.
     * Inject UserServiceInterface as a dependency
     *
     * @param UserServiceInterface $userService
     */
    public function __construct(UserServiceInterface $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function registerUser(Request $request)
    {
        $validateRequest = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
        ]);

        if ($validateRequest->fails()) {
            return response()->json([
                'message' => 'Required fields are missing',
                'status' => false,
                'errors' => $validateRequest->errors(),
            ], 422);
        }

        $user = $this->userService->registerUser($request->all());

        if ($user) {
            return response()->json([
                'message' => 'User created successfully',
                'status' => true,
                'data' => [
                    'token' => $user->createToken('API TOKEN')->plainTextToken,
                    'token_type' => 'Bearer',
                    'user' => $user,
                ],
            ]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginUser(Request $request)
    {
        $validateRequest = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validateRequest->fails()) {
            return response()->json([
                'message' => 'Required fields are missing',
                'status' => false,
                'errors' => $validateRequest->errors(),
            ], 422);
        }

        if (!Auth::attempt($request->only(['email', 'password']))) {
            return response()->json([
                'message' => 'Email or Password does not match',
                'status' => false
            ], 401);
        }

        return response()->json([
            'message' => 'Login successful',
            'status' => true,
            'data' => [
                'token' => Auth::user()->createToken('API TOKEN')->plainTextToken,
                'token_type' => 'Bearer',
                'user' => Auth::user(),
            ],
        ]);
    }
}
