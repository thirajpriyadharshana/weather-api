<?php

namespace App\Providers;

use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryInterface;
use App\Services\User\UserService;
use App\Services\User\UserServiceInterface;
use App\Services\Weather\WeatherService;
use App\Services\Weather\WeatherServiceInterface;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /**
         * Registering services with Laravel IoC
         */
        App::bind(WeatherServiceInterface::class, WeatherService::class);
        App::bind(UserServiceInterface::class, UserService::class);

        /**
         * Registering repositories with Laravel IoC
         */
        App::bind(UserRepositoryInterface::class, UserRepository::class);


    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
