<?php


namespace App\Services\Weather\DataSources\APIClient;


/**
 * Class APIResponse
 * @package App\Services\Weather\DataSources\APIClient
 */
class APIResponse
{
    /**
     * @var array
     */
    public $data;
    /**
     * @var string
     */
    public $message;
    /**
     * @var bool
     */
    public $success;
    /**
     * @var int
     */
    public $httpStatusCode;

    /**
     * APIResponse constructor.
     * @param array $data
     * @param string $message
     * @param bool $success
     * @param int $httpStatusCode
     */
    function __construct($data = [], $message = '', $success = true, $httpStatusCode = 200)
    {
        $this->data = $data;
        $this->message = $message;
        $this->success = $success;
        $this->httpStatusCode = $httpStatusCode;
    }

    /**
     * Convert response to array
     * @return array
     */
    public function toArray()
    {
        return (array)$this;
    }
}
